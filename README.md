# INCUB API
## GET STARTED
Download in-cub and in-cub-api on gitlab repository

INCUB FRONT-END --> https://gitlab.com/awesome-mds/in-cub

INCUB BACK-END --> https://gitlab.com/awesome-mds/in-cub-api

## LOCAL
__STEPS:__

1. Run your mongodb database
2. Start a connection on `localhost:27017`
3. Start server with `node index.js`
4. Start in-cub front-end app
5. go on `localhost:4200`
5. Sign-up/Sign-in
6. Play with the app :)

