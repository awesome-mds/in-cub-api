let express = require('express');
let bodyParser = require("body-parser");
let mongoose = require('mongoose');
let jwt = require('express-jwt');
let jwks = require('jwks-rsa');
let request = require("request");
let cors = require('cors')
 
// # Server config
let hostname = 'localhost'; 
let port = process.env.PORT || 8080;
// # END Server config

// # Database config
let options = { server: { socketOptions: { keepAlive: 300000, connectTimeoutMS: 30000 } }, 
replset: { socketOptions: { keepAlive: 300000, connectTimeoutMS : 30000 } } };
let databaseUrl = "mongodb://localhost:27017/in-cub"; 
 
mongoose.connect(databaseUrl, options);
let db = mongoose.connection; 
db.on('error', console.error.bind(console, 'Database connection error :( ')); 
db.once('open', () => {
    console.log("We are connected to the database :D"); 
});
// # END Database config

let app = express();

// # JWT config
var checkJwt = jwt({
    secret: jwks.expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 100,
        jwksUri: 'https://dev-z6xxqkef.auth0.com/.well-known/jwks.json'
  }),
  audience: 'http://localhost:8080',
  issuer: 'https://dev-z6xxqkef.auth0.com/',
  algorithms: ['RS256']
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}))
app.use(cors({credentials: true, origin: true}))
// # END JWT config


let router = express.Router();

// # AUTHENTIFICATION
// router.route('/api/public/login')
//     .get((req, res) => res.send("hello world"));

// router.route('/api/public/logout')
//     .get((req, res) => res.send("hello world"));
// # END AUTHENTIFICATION

// # ADVISOR
let advisorSchema = mongoose.Schema({
    lastName: String,
    firstName: String,
    description: String
});

let Advisor = mongoose.model('Advisor', advisorSchema);

router.route('/api/private/advisors')
    .get(checkJwt, (req, res) => { 
        Advisor.find({}, (err, advisors) => {
            if (err) { res.send(err); }
            res.json(advisors)
        });
    })
    .post(checkJwt, (req, res) => {
        let new_advisor = new Advisor();
        new_advisor.lastName = req.body.lastName;
        new_advisor.firstName = req.body.firstName;
        new_advisor.description = req.body.description;

        new_advisor.save(error => {
            if (error) { res.send(error); }
            res.send({message: 'Successfully saved'});
        })
    });

router.route('/api/private/advisors/:id')
    .put(checkJwt, (req, res) => {
        Advisor.findById(req.params.id, (err, advisor) => {
            if (err) {
                res.send(err); 
            } else {
                if (req.body.lastName) {
                    advisor.lastName = req.body.lastName;
                }
                if (req.body.firstName) {
                    advisor.firstName = req.body.firstName;
                }
                if (req.body.description) {
                    advisor.description = req.body.description;
                }
                advisor.save(err => {
                    if (err) { res.send(err); }
                    res.send({ message: 'Successfully updated'});
                });
            }
        });
    })
    .delete(checkJwt, (req, res) => {
        Advisor.deleteOne({_id: req.params.id}, (err) => {
            if (err) { res.send(err); }
            res.send({message: 'Successfully deleted'});
        })
    });
// # END ADVISOR

// # STARTUP
let startupSchema = mongoose.Schema({
    name: String,
    businessLine: String,
    legalRepresentative: String,
    coFoundersNumber: Number,
    description: String,
    address: String,
    advisor: advisorSchema
});

let Startup = mongoose.model('Startup', startupSchema);

router.route('/api/private/startups')
    .get(checkJwt, (req, res) => { 
        Startup.find({}, (err, advisors) => {
            if (err) { res.send(error); }
            res.json(advisors)
        });
    })
    .post(checkJwt, (req, res) => {
        let new_startup = new Startup();
        new_startup.name = req.body.name;
        new_startup.businessLine = req.body.businessLine;
        new_startup.legalRepresentative = req.body.legalRepresentative;
        new_startup.coFoundersNumber = req.body.coFoundersNumber;
        new_startup.description = req.body.description;
        new_startup.address = req.body.address;
        new_startup.advisor = req.body.advisor;

        new_startup.save(error => {
            if (error) { res.send(error); }
            res.send({message: 'Successfully saved'});
        })
    });

router.route('/api/private/startups/:id')
    .put(checkJwt, (req, res) => {
        Startup.findById(req.params.id, (err, startup) => {
            if (err) { 
                res.send(err); 
            } else {
                if (req.body.name) {
                    startup.name = req.body.name;
                }
                if (req.body.businessLine) {
                    startup.businessLine = req.body.businessLine;
                } 
                if (req.body.legalRepresentative) {
                    startup.legalRepresentative = req.body.legalRepresentative;
                }
                if (req.body.coFoundersNumber) {
                    startup.coFoundersNumber = req.body.coFoundersNumber;
                }
                if (req.body.description) {
                    startup.description = req.body.description;
                }
                if (req.body.address) {
                    startup.address = req.body.address;
                }
                if (req.body.advisor) {
                    startup.advisor = req.body.advisor;
                }
                startup.save(err => {
                    if (err) { res.send(err); }
                    res.send({message: 'Successfully updated'});
                });
            }
        });
    })
    .delete(checkJwt, (req, res) => {
        Startup.deleteOne({_id: req.params.id}, (err) => {
            if (err) { res.send(err); }
            res.send({ message: 'Successfully deleted' });
        })
    });
// # END STARTUP

app.use(router);

app.listen(port, hostname, () => {
	console.log("Server listening on http://"+ hostname +":"+port+"\n"); 
});
